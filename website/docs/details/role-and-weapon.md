# Role and Weapon code names



#### Below are the characters and weapons available for use in the game(demo version).

![image-20221103092644955](role-and-weapon/image-20221103092644955.png)

| characters   | name                                                         |
| ------------ | ------------------------------------------------------------ |
| Goblin       | Goblin_Axe, Goblin_Rogue, Goblin_Axe_B, Goblin_Rogue_B, Goblin_Rogue_Elf, Goblin_Rogue_Twin, Goblin_Rogue_Dual |
| Goblin Small | GoblinSmall_ThrowBomb, GoblinSmall_ThrowKnife, GoblinSmall_Mace |
| Skel         | SkelSmall_Axe, SkelSmall_Mace, SkelSmall_Sword, SkelSmall_Axe_B, SkelSmall_Mace_B, SkelSmall_Sword_B, Skeleton_RSword_Shield |
| Elf          | Elf_Blade, Elf_Blade_B, Elf_DualSword, Elf_DualSword_B, ElfRogue_Archer |





| Weapon        | name                                                         |
| ------------- | ------------------------------------------------------------ |
| melee weapons | Arming_Dagger，Arming_Sword，Dagger_Bleed，Falchion_Simple，Falchion_Long，Mace_Simple，Irish_Long_Sword，Rouelle_Dagger，Spear_Simple，Viking_Battle_Axe，Viking_Sword，Goblin_Rogue |

