---
title: GunScript
---

The gun script is part of the official ModProj project. It is used in the `GunAK47` & `Gun_UMP` weapons.
Checkout the code here: 
* [Gun Scripts](https://github.com/fonzieyang/BTModToolkit/tree/master/ModProj/Assets/Build/Gun_UMP/Script)